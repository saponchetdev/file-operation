       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-EMP1.
       AUTHOR. Saponchet.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT EMP-FILE ASSIGN TO "emp1.dat"
           ORGANIZATION IS  LINE SEQUENTIAL. 

       DATA DIVISION. 
       FILE SECTION. 
       FD EMP-FILE.
       01  EMP-DETAILS.
           88 END-OF-EMP-FILE      VALUE HIGH-VALUE.
           05 EMP-SSN              PIC 9(9).
           05 EMP-NAME.
              10 EMP-FORENAME      PIC X(15).
              10 EMP-SURNAME       PIC X(20).
           05 EMP-DATE-OF-BIRTH.
              10 EMP-YOB           PIC 9(4).
              10 EMP-MOB           PIC 99.
              10 EMP-DOB           PIC 99.
           05 EMP-GENDER           PIC X.
      
       PROCEDURE DIVISION .
       BEGIN.
           OPEN OUTPUT EMP-FILE 
              MOVE "999999999" TO EMP-SSN 
              MOVE "SAPONCHET" TO EMP-FORENAME 
              MOVE "WARAKIDSATHAPOHN" TO EMP-SURNAME
              MOVE "19990624" TO EMP-DATE-OF-BIRTH.
              MOVE "M" TO EMP-GENDER 
              WRITE EMP-DETAILS 

              MOVE "888888888" TO EMP-SSN 
              MOVE "TITAYANEE" TO EMP-FORENAME
              MOVE "KIJNEECHEE" TO EMP-SURNAME
              MOVE "20010103" TO EMP-DATE-OF-BIRTH
              MOVE "F" TO EMP-GENDER 
              WRITE EMP-DETAILS 

              MOVE "000000000" TO EMP-SSN 
              MOVE "WORAWIT" TO EMP-FORENAME
              MOVE "WERAPAN" TO EMP-SURNAME
              MOVE "19780923" TO EMP-DATE-OF-BIRTH
              MOVE "M" TO EMP-GENDER 
              WRITE EMP-DETAILS 
           CLOSE EMP-FILE 
           GOBACK 
       .