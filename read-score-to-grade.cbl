       IDENTIFICATION DIVISION. 
       PROGRAM-ID. READ-SCORE-TO-GRADE.
       AUTHOR. Saponchet

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION.
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           
           SELECT GRADE-FILE ASSIGN TO "grade.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD SCORE-FILE.
       01  SCORE-DETAILS.
           88 END-OF-SCORE-FILE VALUE HIGH-VALUE.
           05 STU-ID         PIC X(8).
           05 MIDTERM-SCORE  PIC 99V99.
           05 FINAL-SCORE    PIC 99V99.
           05 PROJECT-SCORE  PIC 99V99.

       FD  GRADE-FILE.
       01  GRADE-DETAILS.
           05 STU-ID         PIC X(8).
           05 SUM-SCORE      PIC 999V99.
           05 GRADE          PIC XX.

       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT SCORE-FILE 
           OPEN OUTPUT GRADE-FILE 

           PERFORM UNTIL END-OF-SCORE-FILE 
              READ SCORE-FILE 
                 AT END SET END-OF-SCORE-FILE TO TRUE
              END-READ
              IF NOT END-OF-SCORE-FILE THEN
                 DISPLAY "--------------------"
                 PERFORM 001-PROCESS THRU 002-EXIT
              END-IF 
           END-PERFORM

           CLOSE SCORE-FILE 
           CLOSE GRADE-FILE 
           GOBACK 
       .
       001-PROCESS.
           MOVE STU-ID IN SCORE-DETAILS TO STU-ID IN GRADE-DETAILS 
           COMPUTE SUM-SCORE = MIDTERM-SCORE + FINAL-SCORE 
                               + PROJECT-SCORE 
           
           EVALUATE TRUE 
              WHEN  SUM-SCORE >= 80 MOVE "A" TO GRADE 
              WHEN  SUM-SCORE >= 75 MOVE "B+" TO GRADE 
              WHEN  SUM-SCORE >= 70 MOVE "B" TO GRADE 
              WHEN  SUM-SCORE >= 65 MOVE "C+" TO GRADE 
              WHEN  SUM-SCORE >= 60 MOVE "C" TO GRADE 
              WHEN  SUM-SCORE >= 55 MOVE "D+" TO GRADE 
              WHEN  SUM-SCORE >= 50 MOVE "D" TO GRADE 
              WHEN  OTHER MOVE "F" TO GRADE

           END-EVALUATE
           DISPLAY "STU-ID: " STU-ID IN SCORE-DETAILS 
           DISPLAY "SUM-SCORE: " SUM-SCORE
           DISPLAY "GRADE: " GRADE 

           WRITE GRADE-DETAILS 
       .
       002-EXIT.
           EXIT
       .