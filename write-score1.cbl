       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-GRADE1.
       AUTHOR. Saponchet.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD SCORE-FILE.
       01  SCORE-DETAILS.
           05 STU-ID      PIC X(8).
           05 MIDTERM-SCORE  PIC 99V99.
           05 FINAL-SCORE  PIC 99V99.
           05 PROJECT-SCORE  PIC 99V99.
       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT SCORE-FILE
              MOVE "62160307" TO STU-ID 
              MOVE "29.75" TO MIDTERM-SCORE
              MOVE "32.50" TO FINAL-SCORE
              MOVE "15.25" TO PROJECT-SCORE
              WRITE SCORE-DETAILS 

              MOVE "62010044" TO STU-ID 
              MOVE "38.75" TO MIDTERM-SCORE
              MOVE "40.00" TO FINAL-SCORE
              MOVE "18.75" TO PROJECT-SCORE
              WRITE SCORE-DETAILS 

              MOVE "62080041" TO STU-ID 
              MOVE "35.75" TO MIDTERM-SCORE
              MOVE "34.50" TO FINAL-SCORE
              MOVE "18.75" TO PROJECT-SCORE
              WRITE SCORE-DETAILS

              MOVE "64520023" TO STU-ID 
              MOVE "20.00" TO MIDTERM-SCORE
              MOVE "19.25" TO FINAL-SCORE
              MOVE "10.50" TO PROJECT-SCORE
              WRITE SCORE-DETAILS
           CLOSE SCORE-FILE 
           GOBACK 
           .